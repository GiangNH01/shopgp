@extends('layout')

@section('title', 'Giỏ hàng')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{URL::to('/')}}">Trang chủ </a></li>
				  <li class="active">Giỏ hàng</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<?php
					$content = Cart::content();
				?>

				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Sản phẩm</td>
							<td class="description"></td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng tiền</td>
							<td class="total"></td>
						</tr>
					</thead>
					<tbody>
						@foreach($content as $v_content)
						<tr>
							<td class="cart_product">
								<img src="{{URL::to('uploads/product/'.$v_content->options->image)}}" width="90" alt="" />
							</td>
							<td class="cart_description">
								<h4>{{$v_content->name}}</h4>
								<p>Mã sản phẩm: {{$v_content->id}}</p>
							</td>
							<td class="cart_price">
								<p>{{number_format($v_content->price).' '.'vnđ'}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<input class="cart_quantity_input" type="number" name="cart_quantity" value="{{$v_content->qty}}"  >
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php
										$subtotal = $v_content->price * $v_content->qty;
										echo number_format($subtotal).' '.'vnđ';
									?>

								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_edit" href="{{URL::to('/update-cart-quantity/'.$v_content->rowId)}}"><i class="fa fa-edit"></i></a>
								<a class="cart_quantity_delete" href="{{URL::to('/delete-to-cart/'.$v_content->rowId)}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						@endforeach
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Giá tạm tính</td>
										<td>{{Cart::total().' '.'vnđ'}}</td>
									</tr>
									<tr>
										<td>Thuế</td>
										<td>{{Cart::tax().' '.'vnđ'}}</td>
									</tr>
									<tr class="shipping-cost">
										<td>Phí vận chuyển</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Tổng tiền</td>
										<td><span>{{Cart::total().' '.'vnđ'}}</span></td>
									</tr>
									<tr>
										<td></td>
										<td>
											<a class="btn btn-default update" href="{{URL::to('/update-cart-quantity')}}">Canle</a>
											<a class="btn btn-default check_out" href="{{URL::to('/checkout')}}">Thanh toán</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			
			</div>
		</div>
	</section>
@endsection