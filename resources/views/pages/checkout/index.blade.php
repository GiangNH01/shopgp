@extends('layout')

@section('title', 'Thanh toán')

@section('content')
<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{URL::to('/')}}">Trang chủ</a></li>
				  <li class="active">Thanh toán giỏ hàng</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="shopper-informations">
				<div class="row">
					<form action="{{URL::to('/save-checkout-customer')}}" method="POST">
						{{csrf_field()}}
					<div class="col-sm-5">
						<div class="shopper-info">
							<p>Điền thông tin gửi hàng</p>
								
									<input type="email" name="shipping_email" placeholder="Email" required title="ví dụ: abc@gmail.com">
									<input type="text" name="shipping_name" placeholder="Họ và tên" required pattern="[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ \\s]{8,}" title="Tên không hợp lệ">
									<input type="text" name="shipping_address" placeholder="Địa chỉ" required pattern="[a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ \\s]{10,100}" title="Địa chỉ gồm có: số nhà - đường - xã/phường - quận/huyện - tỉnh/thành phố">
									<input type="tel" name="shipping_phone" placeholder="Phone" pattern="[0-9]{10}" required title="ví dụ: 0329849797">
									
									<input type="submit" value="Gửi" name="send_order" class="btn btn-primary btn-sm">
								
						</div>
					</div>
					
					<div class="col-sm-7">
						<div class="order-message">
							<p>Ghi chú</p>
							<textarea name="shipping_notes"  placeholder="Ghi chú đơn hàng của bạn" rows="16"></textarea>
							
						</div>	
					</div>
					</form>					
				</div>
			</div>
			<div class="review-payment">
				<h2>Sản phẩm của bạn</h2>
			</div>

			<div class="table-responsive cart_info">
				<?php
					$content = Cart::content();
				?>
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Sản phẩm</td>
							<td class="description"></td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng tiền</td>
						</tr>
					</thead>
					<tbody>
						@foreach($content as $v_content)
						<tr>
							<td class="cart_product">
								<img src="{{URL::to('uploads/product/'.$v_content->options->image)}}" width="90" alt="" />
							</td>
							<td class="cart_description">
								<h4>{{$v_content->name}}</h4>
								<p>Mã sản phẩm: {{$v_content->id}}</p>
							</td>
							<td class="cart_price">
								<p>{{number_format($v_content->price).' '.'vnđ'}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<input class="cart_quantity_input" type="number" name="cart_quantity" value="{{$v_content->qty}}"  >
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php
										$subtotal = $v_content->price * $v_content->qty;
										echo number_format($subtotal).' '.'vnđ';
									?>

								</p>
							</td>
						</tr>
						@endforeach
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Giá tạm tính</td>
										<td>{{Cart::total().' '.'vnđ'}}</td>
									</tr>
									<tr>
										<td>Thuế</td>
										<td>{{Cart::tax().' '.'vnđ'}}</td>
									</tr>
									<tr class="shipping-cost">
										<td>Phí vận chuyển</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Tổng tiền</td>
										<td><span>{{Cart::total().' '.'vnđ'}}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->
@endsection