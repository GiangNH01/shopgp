@extends('layout')

@section('title', 'Thông tin đơn hàng')

@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{URL::to('/')}}">Trang chủ</a></li>
				  <li class="active">Thanh toán giỏ hàng</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Thanh toán thành công</h2>
			</div>
			<div class="checkout-options">
				<h3>Cảm ơn bạn đã ghé shop ủng hộ</h3>
			</div><!--/checkout-options-->

			<div class="step-one">
				<h2 class="heading">Thông tin đơn hàng của bạn</h2>
			</div>

			<div class="shopper-informations">
				
			</div>
			<div class="review-payment">
				<h2>Danh sách sản phẩm</h2>
			</div>

			<div class="table-responsive cart_info">
				<?php
					$content = Cart::content();
				?>
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Sản phẩm</td>
							<td class="description"></td>
							<td class="price">Giá</td>
							<td class="quantity">Số lượng</td>
							<td class="total">Tổng tiền</td>
						</tr>
					</thead>
					<tbody>
						@foreach($content as $v_content)
						<tr>
							<td class="cart_product">
								<img src="{{URL::to('uploads/product/'.$v_content->options->image)}}" width="90" alt="" />
							</td>
							<td class="cart_description">
								<h4>{{$v_content->name}}</h4>
								<p>Mã sản phẩm: {{$v_content->id}}</p>
							</td>
							<td class="cart_price">
								<p>{{number_format($v_content->price).' '.'vnđ'}}</p>
							</td>
							<td class="cart_quantity">
								<p>{{$v_content->qty}}</p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php
										$subtotal = $v_content->price * $v_content->qty;
										echo number_format($subtotal).' '.'vnđ';
									?>

								</p>
							</td>
						</tr>
						@endforeach
						<tr>
							<td colspan="3">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									
									<tr>
										<td>Số tiền bạn cần trả là:</td>
										<td><span>{{Cart::total().' '.'vnđ'}}</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="payment-options">
				<a href="{{URL::to('/')}}" class="btn btn-primary" href="">Tiếp tục mua hàng</a>
			</div>
		</div>
	</section> <!--/#cart_items-->
@endsection